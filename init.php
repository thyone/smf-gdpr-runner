<?php
//Only run this thing from the command line!
if (php_sapi_name() !='cli') exit;

require 'vendor/autoload.php';
use Symfony\Component\Dotenv\Dotenv;

//Load environment variables
$dotenv = new Dotenv();
$dotenv->load('.env');

try {
	$db = new PDO('mysql:dbname=' . getenv('DB_DATABASE') . ';host=' . getenv('DB_HOST'), getenv('DB_USER'), getenv('DB_PASS'));
} catch (\PDOException $e) {
	throw new \PDOException($e->getMessage(), (int)$e->getCode());
}


if(!getenv('GDPR_NAME') || strlen(getenv('GDPR_NAME')) <= 7)
	die('You need to set a GDPR_NAME variable in the .env file. Make sure it is at least 8 characters long so we won\'t be too greedy when removing texts.');
