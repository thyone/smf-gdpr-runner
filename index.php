<?php
require 'init.php';
require 'functions.php';

//Get results, ordered by id DESC so we can count down towards the end...
$result = $db->query('SELECT id_msg, body, modified_name FROM ' . getenv('SMF_MESSAGES') . ' ORDER BY id_msg DESC');

$to_update = [];

while($row = $result->fetchObject()){
	echo 'Checking message ID #' . $row->id_msg . "                    \r";

	if(fieldNeedsCleaning($row->body, getenv('GDPR_NAME')) || fieldNeedsCleaning($row->modified_name, getenv('GDPR_NAME'))){
		echo PHP_EOL . 'Cleaning message fields #' . $row->id_msg . '...';
		$to_update[$row->id_msg] = ['body' => cleanField($row->body, getenv('GDPR_NAME')), 'modified_name' => cleanField($row->modified_name, getenv('GDPR_NAME'))];
		echo ' done' . PHP_EOL;
	}
}

echo PHP_EOL;

echo 'Now updating all ' . count($to_update) . ' rows that contain "' . getenv('GDPR_NAME') . '"...' . PHP_EOL;

$stmt = $db->prepare('UPDATE ' . getenv('SMF_MESSAGES') . ' SET body = ?, modified_name = ? WHERE id_msg = ?');

if(getenv('RUNNER_TEST_ONLY') != 'commit')
	die('Runner in test-only mode. I would have updated ' . count($to_update) . ' rows.' . PHP_EOL);


$i = 0;
foreach($to_update as $id => $fields){
	$i++;
	echo $i . "\r";

	$stmt->execute([$fields['body'], $fields['modified_name'], $id]);
}

echo PHP_EOL . 'All done!' . PHP_EOL;

