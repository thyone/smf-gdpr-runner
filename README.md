# SMF GDPR Runner

This simple script runs over your `smf_messages` table (configurable) and deletes a name (or other string) from the `body` column when necessary.

## Getting Started

Follow these instructions to get this script running. Make sure to only run this from the command line, as execution may take a long time if you have a lot of messages.

### Prerequisites

```
PHP >= 7.0, Composer
```

### Install Composer

If you do not have composer installed, please install it first (you can do that in the directory where you cloned this project): see [Composer website](https://getcomposer.org/download)

### Install dependencies

```
./composer.phar install
```

### Copy & set the environment variables

Copy the .env.example file to .env:

```
cp .env.example .env
```

Edit the .env file to reflect your database connection and check that `GDPR_NAME` is set to the string you want to have removed from the post body.

If you want you can also edit the `GDPR_REPLACE` string.

### Getting out of test-only mode

By default the runner works in test-only mode. This will not commit changes to the database, but only show you how many rows would be updated.

To allow actually committing the data, change the following line in .env (case sensitive):

```
RUNNER_TEST_ONLY=commit
``` 

## Running the script

Run the script by executing

```
php index.php
```

## Back-up your data

Please make sure to test this script first on a local copy of the database, and always make sure you have a back-up of your live database at hand!

## Changes
(2019-08-20) Updated script to include 'modified_name' field which may contain the user's name.

## Authors

* **Peter Bosch**

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

