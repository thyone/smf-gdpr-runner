<?php

function fieldNeedsCleaning($body, $gdpr_name){
	$matches = preg_match('/' . $gdpr_name . '/i', $body);
	return $matches > 0 ? true : false;
}

function cleanField($body, $gdpr_name){
	return preg_replace('/' . $gdpr_name . '/i', getenv('GDPR_REPLACE'), $body);
}
